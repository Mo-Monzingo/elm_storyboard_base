module MarkdownDeserialize exposing (asMarkdown)

import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import Html.Lazy exposing (..)
import Markdown


markdownOptions : Markdown.Options
markdownOptions =
    { githubFlavored = Just { tables = False, breaks = False }
    , defaultHighlighting = Nothing
    , sanitize = False
    , smartypants = True
    }


asMarkdown : List (Html.Attribute msg) -> String -> Html msg
asMarkdown attrs str =
    Markdown.toHtmlWith markdownOptions attrs str
