module Rest exposing (login)

import Adapter.AuthEmail
import Http
import Json.Decode
import Model exposing (..)
import Msg exposing (Msg)
import RemoteData exposing (RemoteData)


login : () -> Cmd Msg
login ignored =
    Cmd.none



-- login : () -> Cmd Msg
-- login ignored =
--     Http.post
--         { url = "/some/url"
--         , body = Http.jsonBody <| Adapter.AuthEmail.encodeLogin { email = Maybe.withDefault "" userModel.userEnteredLoginEmail }
--         , expect = Http.expectJson Msg.LoginResult Adapter.AuthEmail.redirect
--         }
