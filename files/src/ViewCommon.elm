module ViewCommon exposing (checkmark, nbsp)

import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import Html.Lazy exposing (..)
import Model exposing (..)
import Msg exposing (Msg(..))
import Route


checkmark : String
checkmark =
    String.fromChar '✓'


nbsp : String
nbsp =
    String.fromChar '\u{00A0}'
