const {content: content_general} = require('./content/content_english.js');

let options = 'usa';

let content = {
  version: 1,
  img_example: require('./content/img/example.png'),
};

Object.assign(content, content_general);

export {content, options};
