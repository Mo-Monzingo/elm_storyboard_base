module Adapter.Content exposing (ContentData, decode)

import Html exposing (Html)
import Html.Attributes exposing (class)
import Json.Decode as Decode
import Json.Decode.Pipeline as Jpipe
import MarkdownDeserialize


type alias ContentData =
    { version : Int

    -- NOTE: ALL Type Alias FIELDS MUST BE SORTED ALPHABETICAL or the decoding will mix Strings up (and may or may not fail, which is the worst)
    -- NOTE: MUST BE ALPHABETICAL with version first
    , example_content : Html Never
    , img_example : String
    }


decode : Decode.Decoder ContentData
decode =
    Decode.succeed ContentData
        |> Jpipe.required "version" Decode.int
        -- NOTE: MUST BE ALPHABETICAL with version first
        |> Jpipe.required "example_content" (asMarkdown [ class "markdown" ])
        |> Jpipe.required "img_example" Decode.string


asMarkdown : List (Html.Attribute msg) -> Decode.Decoder (Html msg)
asMarkdown attrs_ =
    Decode.string |> Decode.andThen (Decode.succeed << MarkdownDeserialize.asMarkdown attrs_)



-- arrayAsTuple2 : Decode.Decoder a -> Decode.Decoder b -> Decode.Decoder ( a, b )
-- arrayAsTuple2 a b =
--     Decode.index 0 a
--         |> Decode.andThen (\aVal -> Decode.index 1 b |> Decode.andThen (\bVal -> Decode.succeed ( aVal, bVal )))
