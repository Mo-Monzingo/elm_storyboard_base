module ViewStoryCell exposing (view, viewFull)

import Browser
import Common exposing (dname)
import Dict exposing (Dict)
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import Html.Lazy exposing (..)
import Json.Decode
import MarkdownDeserialize exposing (asMarkdown)
import Model exposing (..)
import Msg exposing (Msg)
import Route
import Set exposing (Set)


cells : ContentModel -> List CellRec
cells contentModel =
    [ normal { cellType = Title { title = "Storyboard" } }
    , sectionStart { cellType = TitlePlusSubtitle { title = "First Behavior", subtitle = "with a subtitle" } }
    , normal { cellType = TitlePlusSubtitleWithText { title = "We should do something", subtitle = "with a subtitle", strs = [ "and with notes", "below the subtitle" ] } }
    , normal { cellType = Custom (div [ class "font-semibold text-xl" ] [ text "custom html" ]) }
    , concept { cellType = Email (div [ class "font-semibold text-xl" ] [ text "Insert email here" ]) }
    , normal { cellType = Placeholder { title = "Placeholder for something" } }
    , summary { cellType = Website { url = "https://www.google.com" } }
    , normal { cellType = Img { url = contentModel.content.img_example } }
    ]


type CellType
    = NoOp
    | Custom (Html Msg)
    | Email (Html Msg)
    | Img { url : String }
    | UserData { users : List String }
    | Placeholder { title : String }
    | Title { title : String }
    | TitlePlusSubtitle { title : String, subtitle : String }
    | TitlePlusSubtitleWithText { title : String, subtitle : String, strs : List String }
    | Website { url : String }


type Maturity
    = MaturityNone
    | MaturityConcept
    | MaturitySummary


type alias CellRec =
    { cellType : CellType, bg : String, maturity : Maturity }


type alias PreCellRec =
    { cellType : CellType }


view : ContentModel -> Model -> Int -> Html Msg
view contentModel model index =
    div [ class "flex" ]
        [ viewCells contentModel model index
        , viewFocus contentModel model index
        ]


viewCells : ContentModel -> Model -> Int -> Html Msg
viewCells contentModel model focusIndex =
    let
        cssRight =
            class "border-r border-gray-300 mr-4"

        sectionWidth =
            case model.storyboardPortion of
                StoryboardPortion1 ->
                    "flex-none w-1/5"

                StoryboardPortion2 ->
                    "flex-none w-2/5"

                StoryboardPortion3 ->
                    "flex-none w-3/5"

                StoryboardPortion4 ->
                    "flex-none w-4/5"

                StoryboardPortion5 ->
                    "flex-none w-full"
    in
    div [ dname "cellContainer", class "flex flex-col relative", class sectionWidth ]
        [ section [ class "flex absolute top-0 right-0 bg-white", cssRight, style "z-index" "100" ]
            [ button [ onClick <| Msg.StoryboardPortion False, class "ml-2" ] [ text "< less" ]
            , button [ onClick <| Msg.StoryboardPortion True, class "ml-2" ] [ text "more >" ]
            , div [ class "mr-4" ] []
            ]
        , List.indexedMap (viewCell contentModel model focusIndex) (cells contentModel)
            |> div
                [ id "cells"
                , dname "cells"
                , class "h-screen overflow-y-auto flex content-start flex-wrap border border-gray-500"
                , cssRight
                ]
        ]


viewCell : ContentModel -> Model -> Int -> Int -> CellRec -> Html Msg
viewCell contentModel model focusIndex index cellRec =
    let
        foo =
            if focusIndex == index then
                focused

            else
                standard

        common =
            class "flex w-64 h-64 p-1 cursor-pointer border overflow-hidden"

        scale =
            style "transform" "scale(0.95)"

        standard =
            div [ common, class "border-gray-500", onClick <| Msg.BrowserLinkCell index ]

        focused =
            div [ id "focusedCell", common, class "border-red-500" ]
    in
    div [ class "flex flex-col", scale ]
        [ foo [ viewFull contentModel model index ]
        , div [ class "flex justify-end text-sm" ] [ text <| String.fromInt index ]
        ]


viewFocus : ContentModel -> Model -> Int -> Html Msg
viewFocus contentModel model index =
    iframe [ dname "focus", class "w-full h-screen", src <| Route.routeToString <| Route.StoryboardFull index ] []


viewFull : ContentModel -> Model -> Int -> Html Msg
viewFull contentModel model index =
    let
        viewCellRec cellRec =
            case cellRec.maturity of
                MaturityConcept ->
                    div [ class "relative" ]
                        [ div [ class "absolute bg-red-500 w-64 text-center text-gray-800", style "left" "-5rem", style "top" "2rem", style "transform" "rotate(-0.125turn)" ] [ text "Concept" ]
                        , viewCellRecInner cellRec
                        ]

                MaturitySummary ->
                    -- div [ class "relative w-full h-full" ]
                    --     [ div [ class "absolute top-0 right-0 overflow-hidden" , style "z-index" "9999" , style "width" "120px" , style "height" "120px" ]
                    --         [ div [ class "absolute top-0 right-0 overflow-hidden" , style "width" "120px" , style "height" "120px" , style "transform" "rotate(45deg)" ]
                    --             [ div [ class "w-full bg-green-300 text-black text-center text-sm" ] [ text "Summary" ] ]
                    --         ]
                    --     , div [ class "overflow-auto" ] [ viewCellRecInner cellRec ]
                    --     ]
                    div [ class "relative w-full h-full" ]
                        [ div [ class "absolute bottom-0 right-0 flex justify-end bg-green-300 text-gray-600 text-sm px-1", style "z-index" "9999" ] [ text "Summary" ]
                        , div [ class "overflow-auto" ] [ viewCellRecInner cellRec ]
                        ]

                _ ->
                    viewCellRecInner cellRec

        viewCellRecInner cellRec =
            let
                centered =
                    class "flex flex-col justify-center items-center text-center w-full h-full"
            in
            case cellRec.cellType of
                NoOp ->
                    text ""

                Custom html ->
                    html

                Email html ->
                    html

                Img { url } ->
                    img [ class "overflow-auto", src url ] []

                UserData { users } ->
                    div [ dname "user-data", class "bg-gray-200 w-full h-full p-2" ]
                        [ h2 [ class "flex font-bold text-xl" ] [ text "User Data" ]
                        , h3 [ class "flex" ] [ text "Users:" ]
                        , if List.isEmpty users then
                            div [ class "flex" ] [ text "None" ]

                          else
                            div [ class "flex flex-col text-sm" ] <| List.map (\u -> div [ class "mb-2" ] [ text u ]) users
                        ]

                Placeholder { title } ->
                    div [ dname "title", centered, classList [ ( "bg-yellow-200", String.isEmpty cellRec.bg ), ( cellRec.bg, not <| String.isEmpty cellRec.bg ) ] ]
                        [ div [ class "flex font-bold text-xl" ] [ text title ]
                        ]

                Title { title } ->
                    div [ dname "title", centered, class cellRec.bg ]
                        [ div [ class "flex font-bold text-xl p-2" ] [ text title ]
                        ]

                TitlePlusSubtitle { title, subtitle } ->
                    div [ dname "titlePlusSubtitle", class "p-2", centered, class cellRec.bg ]
                        [ div [ class "flex font-bold text-xl p-2" ] [ text title ]
                        , div [ class "flex font-semibold text-lg mt-8" ] [ text subtitle ]
                        ]

                TitlePlusSubtitleWithText { title, subtitle, strs } ->
                    div [ dname "titlePlusSubtitleWithText", class "p-2", centered, class cellRec.bg ]
                        [ div [ class "flex font-bold text-xl" ] [ text title ]
                        , div [ class "flex font-semibold text-lg mt-8" ] [ text subtitle ]
                        , section [ class "flexmt-4" ] <| List.map (\str -> p [] [ text str ]) strs
                        ]

                Website { url } ->
                    a [ href url, target "blank", class "m-2" ] [ text "launch ", text url ]
    in
    List.drop index (cells contentModel)
        |> List.head
        |> Maybe.map viewCellRec
        |> Maybe.withDefault (text "")


concept : PreCellRec -> CellRec
concept preCellRec =
    { maturity = MaturityConcept, cellType = preCellRec.cellType, bg = "" }


normal : PreCellRec -> CellRec
normal preCellRec =
    { maturity = MaturityNone, cellType = preCellRec.cellType, bg = "" }


problem : PreCellRec -> CellRec
problem preCellRec =
    { maturity = MaturityNone, cellType = preCellRec.cellType, bg = "bg-red-200" }


sectionStart : PreCellRec -> CellRec
sectionStart preCellRec =
    { maturity = MaturityNone, cellType = preCellRec.cellType, bg = "bg-black text-white" }


summary : PreCellRec -> CellRec
summary preCellRec =
    { maturity = MaturitySummary, cellType = preCellRec.cellType, bg = "" }
