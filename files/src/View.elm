module View exposing (view)

import Browser
import Common exposing (dname)
import Dict exposing (Dict)
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import Html.Lazy exposing (..)
import Json.Decode
import MarkdownDeserialize exposing (asMarkdown)
import Model exposing (..)
import Msg exposing (Msg)
import Route
import Set exposing (Set)
import ViewScratch
import ViewStoryCell


type alias RouteText =
    String


type alias RouteUrl =
    String


view : Model -> Browser.Document Msg
view model =
    case model.contentModelResult of
        Err error ->
            { title = "No content yet"
            , body =
                [ h1 [] [ text "Problem with the content flag. Cannot load." ]
                , Html.pre [] [ Html.text error ]
                ]
            }

        Ok contentModel ->
            case model.nav.domain of
                Model.UnknownDomain ->
                    { title = "Storyboard Feedback Prototype"
                    , body =
                        [ case model.version of
                            0 ->
                                div [] <| viewAll contentModel model

                            _ ->
                                text ""
                        ]
                    }


viewAll : ContentModel -> Model -> List (Html Msg)
viewAll contentModel ({ httpModel, nav } as model) =
    let
        versionView =
            div [ class "flex justify-end" ]
                [ button [ class "p-1 mr-2", onClick <| Msg.VersionSet 0, classList [ ( "bg-gray-500", model.version == 0 ) ] ] [ text "storyboard" ]
                ]

        activeView : ( Html Msg, Bool )
        activeView =
            case nav.routeActive of
                Route.NotFound ->
                    ( text "Route not found", True )

                Route.Home ->
                    ( div [] [ versionView, ViewStoryCell.view contentModel model 0 ], False )

                Route.Scratch ->
                    ( ViewScratch.view contentModel model, False )

                Route.Storyboard index ->
                    ( div [] [ ViewStoryCell.view contentModel model index ], False )

                Route.StoryboardFull index ->
                    ( div [ class "h-screen" ] [ ViewStoryCell.viewFull contentModel model index ], False )

        ( viewActive, showWrapper ) =
            activeView
    in
    if showWrapper then
        [ viewDomainSpecificNav nav
        , div [ class "m-4" ] [ viewActive ]
        ]

    else
        [ viewActive
        ]


viewDomainSpecificNav : NavModel -> Html Msg
viewDomainSpecificNav nav =
    let
        list =
            case nav.domain of
                _ ->
                    -- [ Route.Home, Route.Before, Route.After, Route.Example <| Just "example" ]
                    [ Route.Home ]
    in
    viewNav nav.routeActive list


viewLink : String -> RouteUrl -> RouteText -> Html msg
viewLink active strPath routeText =
    let
        activeWithoutQuery =
            withoutQuery active

        withoutQuery routeStr =
            List.head <| String.split "?" routeStr
    in
    li
        [ class "m-3"
        , classList [ ( "active", activeWithoutQuery == withoutQuery strPath ) ]
        ]
        [ a [ href strPath ] [ text routeText ] ]


viewNav : Route.Route -> List Route.Route -> Html msg
viewNav routeActive list =
    let
        active =
            Route.routeToString routeActive
    in
    nav [ attribute "data-name" "top-nav", class "flex flex-wrap items-baseline border-gray-400 border-b text-gray-500" ]
        [ span [ class "font-semibold ml-2" ] [ text "Prototype Navigation" ]
        , ul [ class "flex flex-wrap" ] <|
            List.map (\r -> viewLink active (Route.routeToString r) (Route.routeToText r)) list
        ]


onEnter : msg -> Html.Attribute msg
onEnter msg =
    let
        isEnterKey keyCode =
            if keyCode == 13 then
                Json.Decode.succeed msg

            else
                Json.Decode.fail "silent failure :)"
    in
    on "keyup" <|
        Json.Decode.andThen isEnterKey Html.Events.keyCode
